package com.hunglp.bmo.service;

import com.hunglp.bmo.entity.Domain;

public interface RabbitMQSenderService {
	public void send(Domain domain);
}	
