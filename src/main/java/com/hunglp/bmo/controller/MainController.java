package com.hunglp.bmo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.hunglp.bmo.dao.DomainRepository;
import com.hunglp.bmo.entity.Domain;
import com.hunglp.bmo.service.RabbitMQSenderService;

@Controller
public class MainController {
	
	@Autowired
	RabbitMQSenderService rabbitMQSender;

	@Autowired
	DomainRepository domainRepository;

	@GetMapping("/")
	public String main(Model model) {
		model.addAttribute("message", "hello");

		List<Domain> domains = new ArrayList<>();
		domainRepository.findAll().forEach(l -> domains.add(l));
		for (Domain domain : domains) {
			System.out.println(domain.get_id() + "_" + domain.getDomain());
		}
		return "welcome";
	}
	
	@GetMapping("/producer")
	public String sendMessage() {
		
		List<Domain> domains = new ArrayList<>();
		domainRepository.findAll().forEach(l -> domains.add(l));
		for(Domain domain : domains) {
			rabbitMQSender.send(domain);
		}
		
		System.out.println("send ok");
		return "welcome";
	}

	

}
