package com.hunglp.bmo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hunglp.bmo.entity.Domain;



public interface DomainRepository extends MongoRepository<Domain, Long> {
	
}
